function val = af_time_fun(format,mtf_val,mtf_sta)
format_1 = char(format);
vid_for = ['*.',format_1];
[FileName_vid,PathName_vid] = uigetfile({vid_for},'MultiSelect','on');
addpath(PathName_vid);

    obj = VideoReader(char(FileName_vid));
    frame_rate = obj.FrameRate;
    numFrames = obj.NumberOfFrames;
    figure;
    for k = 1 : numFrames
         frame = read(obj,k);
         imshow(frame);
         imwrite(frame,[PathName_vid,num2str(k),'.jpg'],'jpg');
    end
    inf = ['framte rate = ',num2str(frame_rate)];
    disp(inf)
    close(gcf);

[FileName_pic,PathName_pic] = uigetfile({'*jpg'},'MultiSelect','on');
fol_pat = PathName_pic;
sel_pat_2 = fol_pat;
addpath(fol_pat);

filenames = FileName_pic';
filelength = length(filenames);

io = 1;                                   
del = 1;                                 
weight = ['0.213','0.715','0.072'];      
figure;
img_read = imread(char(filenames(end)));
[cropped,rect] = imcrop(img_read); 
results = cell(filelength+2,12);
header = {'NO.','FILENAMES','R_LP/PH','G_LP/PH','B_LP/PH','Y_LP/PH';
          'NO.','FILENAMES','R_cyc/pix','G_cyc/pix','B_cyc/pix','Y_cyc/pix'};
results(1,1:6) = header(1,:);
results(1,7:12) =  header(2,:); 

for i = 1:filelength
    filename = char(filenames(i));
    img_in = imread(filename);
    a = imcrop(img_in,rect); 
    figure;
    imshow(a);
    name = [sel_pat_2,num2str(i),'_cropped','.png'];
    saveas(gcf,name); 
    %a = imcrop(img_in);  
    
    dat = sfrdata(io,del,weight,a);   
    xlswrite([fol_pat,'\dat.xlsx'],dat,filename);
    dat_temp = zeros(size(dat,1),size(dat,2));
    
    
    for j = 2:5   
        dat_temp(:,1) = abs(dat(:,1)-0.5);
        [nyquist_row,nyquist_col] = find(dat_temp(:,1) == min(dat_temp(:,1)));
        mtf_nor_val = mtf_val/100; 
        dat_temp(:,j) = abs(dat(:,j)-mtf_nor_val);
        [mtf_row,mtf_col] = find(dat_temp(1:nyquist_row,j) == min(dat_temp(1:nyquist_row,j)));
        dat_mtf_fre = dat(mtf_row,1);   
        dat_mtf = dat(mtf_row,j); 
        
        %find data smaller & bigger than corresponding MTF value
            if  dat_mtf == mtf_nor_val   %MTF50 mtf_nor_val = 0.5
                y_mtf = mtf_nor_val;
                x_mtf = dat_mtf_fre;  
            elseif (dat_mtf - mtf_nor_val) > 0   %MTF50 mtf_nor_val = 0.5
                dat_mtf_rig = 1;
                mtf_row_rig = mtf_row;
                while (0.5 - dat_mtf_rig)<0
                    mtf_row_rig = mtf_row_rig+1;
                    dat_mtf_rig = dat(mtf_row_rig,j);
                end   
                % calculate SFR for corresponding MTF value
                x_mtf_fit=[dat(mtf_row,1),dat(mtf_row_rig,1)]; 
                y_mtf_fit=[dat(mtf_row,j), dat_mtf_rig];
                p = polyfit(x_mtf_fit,y_mtf_fit,1);
                y_mtf = mtf_nor_val;
                x_mtf = (y_mtf-p(2))/p(1);     
            elseif (dat_mtf - mtf_nor_val) < 0
                dat_mtf_lef = 0;
                mtf_row_lef = mtf_row;
                while (0.5 - dat_mtf_lef)>0
                    mtf_row_lef = mtf_row_lef-1;
                    dat_mtf_lef = dat(mtf_row_lef,j);
                end
                % calculate SFR for corresponding MTF value
                x_mtf_fit=[dat(mtf_row,1),dat(mtf_row_lef,1)]; 
                y_mtf_fit=[dat(mtf_row,j),dat_mtf_lef];
                p = polyfit(x_mtf_fit,y_mtf_fit,1);
                y_mtf = mtf_nor_val;
                x_mtf = (y_mtf-p(2))/p(1); 
            end

        %output sfr data to results.xlsx
        [img_read_x,img_read_y,~] = size(img_read);
        pic_H = min(img_read_x,img_read_y);
        x_mtf_lp = x_mtf*pic_H;    
        results_data1 = x_mtf_lp;    
        results_data2 = x_mtf;     
        results(i+1,1) = {i}; results(i+1,7) = {i}; 
        results(i+1,2) = filenames(i); results(i+1,8) = filenames(i); 
        results(i+1,j+1) = num2cell(results_data1);   
        results(i+1,j+7) = num2cell(results_data2);
        xlswrite([fol_pat,'\results.xlsx'],results(:,1:6),'LP per PH');
        xlswrite([fol_pat,'\results.xlsx'],results(:,7:10),'cyc per pix');
    end
    disp(i) 
end

%output ratio for corresponding MTF standard (exp, standard = 1000) 
ratio_data = xlsread([fol_pat,'\results.xlsx'],'LP per PH');
n = 0;
for j = 3:6 
      for i = 1:size(ratio_data,1)  
          if ratio_data(i,j) > mtf_sta
             n = n+1;
          end 
      end
      ratio = (n/size(ratio_data,1))*100;
      results(filelength+2,2) = {'ratio'};
      results(filelength+2,j) = {[num2str(ratio),'%']}; 
      n=0;
end
xlswrite([fol_pat,'\results.xlsx'],results,'results');
disp('dat.xlsx output completed!');
hold on

clc;
close all;
val = 1;

end


function [dat] = sfrdata(io,del,weight,a) 
% parameters setting for sfr function
defweight = [0.213   0.715   0.072]; % rgb weigth for Y channel
oldflag = 0;
nbin = 4;
sflag = 0;
pflag=0;
io =1;
del =1;
weight = defweight;
% ROI data for sfr calculate
a= double(a);
[nlin npix ncol] = size(a);
if ncol ==3;
    lum = zeros(nlin, npix);
    lum = weight(1)*a(:,:,1) + weight(2)*a(:,:,2) + weight(3)*a(:,:,3); 
    cc = zeros(nlin, npix*4);
    cc = [ a(:, :, 1), a(:, :, 2), a(:,:, 3), lum];
    cc = reshape(cc,nlin,npix,4);
    a = cc;
    clear cc;
    clear lum;
    ncol = 4; 
end; 
% rotate ROT for adjusting
[a, nlin, npix, rflag] = rotatev2(a);  
loc = zeros(ncol, nlin);
fil1 = [0.5 -0.5];
fil2 = [0.5 0 -0.5];
tleft  = sum(sum(a(:,      1:5,  1),2));
tright = sum(sum(a(:, npix-5:npix,1),2));
if tleft>tright;
    fil1 = [-0.5 0.5];
    fil2 = [-0.5 0 0.5];
end
 test = abs( (tleft-tright)/(tleft+tright) );
 if test < 0.2;
    disp(' ** WARNING: Edge contrast is less that 20%, this can');
    disp('             lead to high error in the SFR measurement.');
 end; 

fitme = zeros(ncol, 3);
slout = zeros(ncol, 1);

 win1 = ahamming(npix, (npix+1)/2);  

for color=1:ncol;                    
    c = deriv1(a(:,:,color), nlin, npix, fil1);

    for n=1:nlin
        loc(color, n) = centroid( c(n, 1:npix )'.*win1) - 0.5;  
    end;

    fitme(color,1:2) = findedge(loc(color,:), nlin);
    place = zeros(nlin,1);
    for n=1:nlin;
        place(n) = fitme(color,2) + fitme(color,1)*n;
        win2 = ahamming(npix, place(n));
        loc(color, n) = centroid( c(n, 1:npix )'.*win2) -0.5;
    end;
    fitme(color,1:2) = findedge(loc(color,:), nlin);

end;                                     
summary{1} = ' '; 
if io > 0;
    midloc = zeros(ncol,1);
    summary{1} = 'Edge location, slope'; 

    for i=1:ncol;
        slout(i) = - 1./fitme(i,1);  
    if rflag==1,                         
        slout(i) =  - fitme(i,1);
    end;
    midloc(i) = fitme(i,2) + fitme(i,1)*((nlin-1)/2);
    summary{i+1} = [midloc(i), slout(i)];
    end
if ncol>2;
    summary{1} = 'Edge location, slope, misregistration (second record, G, is reference)';
    misreg = zeros(ncol,1);
    for i=1:ncol;
        misreg(i) = midloc(i) - midloc(2);
        summary{i+1}=[midloc(i), slout(i), misreg(i)];
        fitme(i,3) = misreg(i);
    end;
end 
end      

nn =   floor(npix *nbin);
mtf =  zeros(nn, ncol);
nn2 =  floor(nn/2) + 1;

if oldflag ~=1;
    disp('Derivative correction')
    dcorr = fir2fix(nn2, 3);   
end
freq = zeros(nn, 1);

for n=1:nn;
    freq(n) = nbin*(n-1)/(del*nn);
end;

freqlim = 1;
if nbin == 1;
    freqlim = 2;
end
nn2out = round(nn2*freqlim/2);

nfreq = n/(2*del*nn); 
win = ahamming(nbin*npix,(nbin*npix+1)/2);     

esf = zeros(nn,ncol);  

for color=1:ncol
    [point, status] = project(a(:,:,color), loc(color, 1), fitme(color,1), nbin);
    esf(:,color) = point;  
    c = deriv1(point', 1, nn, fil2);
    c = c';

    psf(:,color) = c;   

    mid = centroid(c);
    temp = cent(c, round(mid));           
    c = temp;
    clear temp;
    c = win.*c;
    temp = abs(fft(c, nn));
    mtf(1:nn2, color) = temp(1:nn2)/temp(1);
    if oldflag ~=1;
        mtf(1:nn2, color) = mtf(1:nn2, color).*dcorr;
    end
end;     

dat = zeros(nn2out, ncol+1);
for i=1:nn2out;
    dat(i,:) = [freq(i), mtf(i,:)];
end;

disp('dat completed!');
disp('completed!');

end

function [data] = ahamming(n, mid)

data = zeros(n,1);
wid1 = mid-1;
wid2 = n-mid;
wid = max(wid1, wid2);
pie = pi;
for i = 1:n;
	arg = i-mid;
	data(i) = cos( pie*arg/(wid) );
end;
data = 0.54 + 0.46*data;
end

function [b] = cent(a, center);

n = length(a);
b = zeros(n, 1);
mid = round((n+1)/2);
del = round(center - mid);
if del > 0;
     for i = 1:n-del;
       b(i) = a(i + del);
     end;
elseif del < 1;
     for i = -del+1:n;
       b(i) = a(i + del);
     end;
   else b = a;
end;
end

function [loc] = centroid(x)

n   = 1:length(x);
sumx = sum(x);
if sumx < 1e-4;
 loc = 0;
 else loc = sum(n*x)/sumx;
end
end

function  [b] = deriv1(a, nlin, npix, fil)

 b = zeros(nlin, npix);
 nn = length(fil);
 for i=1:nlin;
  temp = conv(fil, a(i,:));
  b(i, nn:npix) = temp(nn:npix);   
  b(i, nn-1) = b(i, nn);
 end
end

function  [slope, int] = findedge(cent, nlin)

 index=[0:nlin-1];
 [slope int] = polyfit(index, cent, 1);          
return
end

function [correct] = fir2fix(n, m);

if nargin<2;
   del=0;
end
correct = ones(n, 1);
m=m-1;
scale = 1;
for i = 2:n;
    correct(i) = abs((pi*i*m/(2*(n+1))) / sin(pi*i*m/(2*(n+1))));
    correct(i) = 1 + scale*(correct(i)-1);
  if correct(i) > 10; 
    correct(i) = 10;
  end;
end;
end

function [point, status] = project(bb, loc, slope, fac)

status =0;
[nlin, npix]=size(bb);
if nargin<4
   fac = 4 ;
end;
big = 0;
nn = npix *fac ;
win = ahamming(nn, fac*loc(1, 1));
slope =  1/slope;
offset =  round(  fac*  (0  - (nlin - 1)/slope )   );
del = abs(offset);
if offset>0 offset=0;
end
barray = zeros(2, nn + del+100);

  for n=1:npix;
  for m=1:nlin;
   x = n-1;
   y = m-1;
   ling =  ceil((x  - y/slope)*fac) + 1 - offset;
   barray(1,ling) = barray(1,ling) + 1;
   barray(2,ling) = barray(2,ling) + bb(m,n);
  end;
 end;

 point = zeros(nn,1);
 start = 1+round(0.5*del); 

  nz =0;
 for i = start:start+nn-1; 
  if barray(1, i) ==0;
   nz = nz +1;
   status = 0;  
   if i==1;
    barray(1, i) = barray(1, i+1);
   else;
    barray(1, i) = (barray(1, i-1) + barray(1, i+1))/2;
   end;
  end;
 end;
 
 if status ~=0;
  disp('                            WARNING');
  disp('      Zero count(s) found during projection binning. The edge ')
  disp('      angle may be large, or you may need more lines of data.');
  disp('      Execution will continue, but see Users Guide for info.'); 
  disp(nz);
 end;

 for i = 0:nn-1; 
  point(i+1) = barray(2, i+start)/ barray(1, i+start);
 end;
return;
end

function out = rotate90(in, n)

if nargin < 2;
 n = 1;
end

nd = ndims(in);
if nd < 1
 error('input to rotate90 must be a matrix');
 return
end

for i = 1:n
 out = r90(in);
 in = out;
end
return
end

function [out] = r90(in)
[nlin, npix, nc] = size(in);
temp = zeros (npix, nlin);
out = zeros (npix, nlin, nc);

for c = 1: nc;

    temp =  in(:,:,c);
    temp = temp.';
    out(:,:,c) = temp(npix:-1:1, :);
                     
end
out = squeeze(out);
end

function [a, nlin, npix, rflag] = rotatev2(a)

dim = size(a);
nlin = dim(1);
npix = dim(2);
a = double(a);

if length(dim) == 3;
    mm = 2;
else mm =1;
end

nn = 3;  
testv = abs(mean(a(end-nn,:,mm))-mean(a(nn,:,mm)));
testh = abs(mean(a(:,end-nn,mm))-mean(a(:,nn,mm)));

 rflag =0;
if testv > testh
 rflag =1;
 a = rotate90(a);
 temp=nlin;
 nlin = npix;
 npix = temp;

end
end